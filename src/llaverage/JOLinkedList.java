/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package llaverage;

/**
 *
 * @author josh
 */
public class JOLinkedList {
    
    Node head=null;
    public JOLinkedList(){head=null;}
    public JOLinkedList(Node h){head=h;}
    public void add(Node n){
        if(head==null) {
            head=n;
        }else{
            Node nextNode=head;
            Node curNode=null;
            while(nextNode!=null){
                curNode=nextNode;
                nextNode=curNode.next;
            }
            curNode.next=n;
        }
    }
    public int getSize(){
        int len=0;
        if(head==null) return len;
        Node n=head;
        while(n!=null){
            len++;
            n=n.next;
        }
        return len;
    }
    public String toString(){//debug
        String ret = "";
        if(head==null){
            return ret;
        }else{
            Node nextNode=head;
            
            while(nextNode!=null){
                ret+=nextNode.data+" ";
                nextNode=nextNode.next;
            }
        }
        return ret;
    }
    
}
class Node{
        Node next;
        int data;
        public Node(){next=null;data=0;}
        public Node(int d){next=null;data=d;}
        public Node(int d, Node n){next=n;data=d;}
    }
