/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package llaverage;

import java.util.StringTokenizer;

/**
 *
 * @author josh
 */
public class LLAverage {
    static JOLinkedList ll=null;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        String data = getData();
        StringTokenizer st = new StringTokenizer(data," ",false);
        while(st.hasMoreTokens()){
            Node n = new Node(Integer.parseInt(st.nextToken()));
            if(ll!=null){
                ll.add(n);
            }else{
                ll = new JOLinkedList(n);
            }
        }
        Node n1=ll.head;
        while (n1 != null) {
            if (average2(n1, 0, 0, 0)) {
                System.out.println("yes");
                System.exit(0);
            } else {
                System.out.println("no");
            }
            System.out.println("rev");
            n1 = n1.next;
        }
    }
    
    public static float average(Node n, int total, int level){
        //System.out.println("Run for "+n.data);
        total+=n.data;
        level++;
        float avg=(float)total/level;
        System.out.println("Total="+total+" level="+level+" avg="+avg);
        if(n.next!=null){
            float avg2=average(n.next,total,level);
            if(avg2==avg){
                return -1;//counterintuitive success value
            }
        }
        if(n.next!=null){
            average(n.next,0,0);
        }
        return avg;
    }
    public static boolean average2(Node n,int total, int level, float testVal){
        level++;
        total+=n.data;
        float avg=(float)total/level;
       System.out.println("Total="+total+" level="+level+" testVal="+testVal+" avg="+avg);
        if(avg==testVal){
            
            return true;
        }else{
             
              testVal=avg;
                while (n.next != null) {
                Node holder = n;
                while (holder.next != null) {
                    if (average2(holder.next, total, level, testVal)) {
                        return true;
                    } else {
                        holder = holder.next;
                    }
                }
                n = n.next;
            }
             
        }
            
        return false;
    }

    public static boolean hasDuplicate(Node n){
        for(int i=0;i<ll.getSize();i++){
            int cur=n.data;
            Node nextNode=n.next;
            while(nextNode !=null){
                if(nextNode.data==cur)return true;
                nextNode=nextNode.next;
            }
        }
        return false;
    }
    
    public static String getData(){
        String data[]=new String[7];
        data[0]="5 5";//yes=5
        data[1]="3 2 7 1";//yes=2
        data[2]="3 8 1 4";//yes=4
        data[3]="5 1 4";//no
        data[4]="9 2 8 4";//no
        data[5]="10 3 8 5 4";//yes=6.5
        data[6]="";
      
        return data[1];
    }
}
 